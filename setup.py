import sys
from pathlib import Path

from setuptools import setup
from setuptools.command.sdist import sdist as SdistCommand

try:
    from setuptools_rust import RustExtension
except ImportError:
    import subprocess

    # Install setuptools-rust
    errno = subprocess.call([sys.executable, "-m", "pip", "install", "setuptools-rust"])
    if errno:
        print("Please install setuptools-rust package")
        raise SystemExit(errno)
    else:
        from setuptools_rust import RustExtension


class CargoModifiedSdist(SdistCommand):
    """Modifies Cargo.toml to use an absolute rather than a relative path
    The current implementation of PEP 517 in pip always does builds in an
    isolated temporary directory. This causes problems with the build, because
    Cargo.toml necessarily refers to the current version of pyo3 by a relative
    path.
    Since these sdists are never meant to be used for anything other than
    tox / pip installs, at sdist build time, we will modify the Cargo.toml
    in the sdist archive to include an *absolute* path to pyo3.
    """

    def make_release_tree(self, base_dir, files):
        import toml

        cargo_loc = Path(base_dir, "Cargo.toml")
        cargo_toml = toml.loads(cargo_loc.read_text())

        abs_pyo3_path = Path(
            Path(__file__).parent,
            cargo_toml["dependencies"]["pyo3"]["path"]
        ).resolve()

        cargo_toml["dependencies"]["pyo3"]["path"] = abs_pyo3_path
        cargo_loc.write_text(toml.dumps(cargo_toml))


setup_requires = ["setuptools-rust>=0.10.1", "wheel"]

setup(
    name="bigpepereader",
    version="0.1.0",
    packages=["src"],
    rust_extensions=[RustExtension("bigpepereader", "Cargo.toml", debug=True)],
    setup_requires=setup_requires,
    zip_safe=False,
    cmdclass={"sdist": CargoModifiedSdist},
)