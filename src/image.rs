use crate::line_of_pixel::LineOfPixel;
use crate::pixel::Pixel;
use crate::utils::unwrap_or_exit;
use rayon::prelude::*;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::path::PathBuf;
use std::process::exit;

pub struct Image {
    ppm_type: String, //P3 ou P6
    height: usize,
    width: usize,
    max_value: usize, //
    pixels: Vec<Pixel>,
}

/// Sequential read
impl Image {

    /// Read ppm file and get parameters ppm_type, height, width, max_value
    /// Return a Vector of Pixels
    fn new_with_file_common<F>(filename: PathBuf, map_functions: HashMap<String, F>) -> Image
    where
        F: Fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel>,
    {


        let mut f = Self::get_file_buffer(filename);
        let (ppm_type, width, height, max_value) = Self::read_header(&mut f);

        //Get the function which match the ppm_type
        let get_pixel_vec_function = match map_functions.get(&ppm_type) {
            Some(v) => v,
            _ => {
                println!("This PPM format is not supported : {}", ppm_type);
                exit(1);
            }
        };

        let pixels = get_pixel_vec_function(f, width, height, max_value);
        Image {
            ppm_type,
            width,
            height,
            max_value,
            pixels,
        }
    }

    /// Invert Pixels
    pub fn invert(&mut self) {
        for p in self.pixels.iter_mut() {
            p.invert()
        }
    }


    /// Grayscale Pixels
    pub fn grayscale(&mut self) {
        for p in self.pixels.iter_mut() {
            p.grayscale()
        }
    }


    /// Save Images
    pub fn save(&self, path: PathBuf) {
        let mut f = unwrap_or_exit(File::create(path));
        let header = format!(
            "{}\n{} {}\n{}\n",
            self.ppm_type, self.width, self.height, self.max_value
        );
        f.write(header.as_bytes()).unwrap();
        match self.ppm_type.as_str() {
            "P3" => self.save_p3(f),
            "P6" => self.save_p6(f),
            _ => unreachable!("Already checked by "),
        }
    }


    /// Save P3 Image
    pub fn save_p3(&self, mut f: File) {
        for pixel in self.pixels.clone() {
            f.write(pixel.display_raw().as_bytes()).unwrap();
        }
    }


    /// Save P6 Image
    pub fn save_p6(&self, mut f: File) {
        let mut v: Vec<u8> = Vec::with_capacity(self.width * self.height);
        for pixel in self.pixels.clone() {
            v.push(pixel.red());
            v.push(pixel.green());
            v.push(pixel.green());
        }
        unwrap_or_exit(f.write(&v));
    }


    /// Stock P3 et P6 functions in Hashmap
    pub fn new_with_file(filename: PathBuf) -> Image {
        let mut functions = HashMap::new();
        let p3: fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel> = Self::get_pixels_vec_p3;
        let p6: fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel> = Self::get_pixels_vec_p6;
        functions.insert("P3".to_string(), p3);
        functions.insert("P6".to_string(), p6);
        Self::new_with_file_common(filename, functions)
    }

    /// Read ppm File
    fn get_file_buffer(path: PathBuf) -> BufReader<File> {
        BufReader::new(unwrap_or_exit(File::open(path)))
    }

    /// Read and Stock first Line of pixels
    fn process_first_line(
        f: &mut BufReader<File>,
        width: usize,
        height: usize,
        max_value: usize,
    ) -> (usize, Vec<Pixel>) {
        let mut buf = String::with_capacity(70);
        f.read_line(&mut buf).unwrap();
        let mut pixels: Vec<Pixel> = vec![Pixel::new(0, 0, 0); width * height];

        let mut i: usize = 0;
        for pixel in LineOfPixel::new(buf.clone(), max_value) {
            pixels[i] = pixel;
            i += 1;
        }
        (i, pixels)
    }

    /// Read header and get parameters
    fn read_header(f: &mut BufReader<File>) -> (String, usize, usize, usize) {
        let mut ppm_type = String::from("");
        let mut width: usize = 0;
        let mut height: usize = 0;
        let mut index: u8 = 0;
        let mut s = String::with_capacity(70);
        let mut buf = String::with_capacity(70);

        while f.read_line(&mut buf).unwrap() != 0 {
            for c in buf.trim_start().chars() {
                if c.is_whitespace() || c == '#' {
                    if !s.trim().is_empty() {
                        match index {
                            0 => ppm_type = s.clone(),
                            1 => width = s.clone().parse().unwrap(),
                            2 => height = s.clone().parse().unwrap(),
                            3 => {
                                let max_value = s.clone().parse().unwrap();
                                return (ppm_type, width, height, max_value);
                            }
                            _ => unreachable!("Bounded by the algorithm itself"),
                        }
                        index += 1;
                    }
                    s.clear();
                    if c == '#' {
                        break;
                    }
                } else {
                    s.push(c);
                }
            }
            buf.clear();
        }
        println!("Something may be wrong with the header.");
        exit(1);
    }

    /// Get P3 Pixels
    fn get_pixels_vec_p3(
        mut f: BufReader<File>,
        width: usize,
        height: usize,
        max_value: usize,
    ) -> Vec<Pixel> {
        let (mut index, mut pixels) = Self::process_first_line(&mut f, width, height, max_value);
        let mut buf = String::with_capacity(70);

        while f.read_line(&mut buf).unwrap() != 0 {
            for pixel in LineOfPixel::new(buf.clone(), max_value) {
                pixels[index] = pixel;
                index += 1;
            }
            buf.clear();
        }
        pixels
    }


    /// Get P6 Pixels
    fn get_pixels_vec_p6(
        mut f: BufReader<File>,
        _: usize,
        _: usize,
        max_value: usize,
    ) -> Vec<Pixel> {
        let chunk_size = 96;
        f.fill_buf()
            .unwrap()
            .par_chunks(chunk_size)
            .flat_map(|x| {
                let mut v: Vec<Pixel> = Vec::with_capacity(chunk_size / 3);
                let (mut r, mut g) = (0u8, 0u8);
                let mut i = 0;
                for value in x {
                    match i {
                        0 => {
                            r = LineOfPixel::normalise_value(*value as usize, max_value);
                            i += 1;
                        }
                        1 => {
                            g = LineOfPixel::normalise_value(*value as usize, max_value);
                            i += 1;
                        }
                        _ => {
                            v.push(Pixel::new(
                                r,
                                g,
                                LineOfPixel::normalise_value(*value as usize, max_value),
                            ));
                            i = 0;
                        }
                    }
                }
                v
            })
            .collect()
    }
}

/// Parallel read
impl Image {
    pub fn par_new_with_file(filename: PathBuf) -> Image {
        let mut functions = HashMap::new();
        let p3: fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel> = Self::par_get_pixel_vec;
        let p6: fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel> = Self::get_pixels_vec_p6;
        functions.insert("P3".to_string(), p3);
        functions.insert("P6".to_string(), p6);
        Self::new_with_file_common(filename, functions)
    }

    pub fn par_new_with_file2(filename: PathBuf) -> Image {
        let mut functions = HashMap::new();
        let p3: fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel> = Self::par_get_pixel_vec2;
        let p6: fn(BufReader<File>, usize, usize, usize) -> Vec<Pixel> = Self::get_pixels_vec_p6;
        functions.insert("P3".to_string(), p3);
        functions.insert("P6".to_string(), p6);
        Self::new_with_file_common(filename, functions)
    }

    fn par_get_pixel_vec(f: BufReader<File>, _: usize, _: usize, max_value: usize) -> Vec<Pixel> {
        f.lines()
            .map(|x| x.unwrap())
            .collect::<Vec<String>>()
            .into_par_iter()
            .flat_map(|x| -> Vec<Pixel> { LineOfPixel::new(x, max_value).collect() })
            .collect()
    }

    fn par_get_pixel_vec2(f: BufReader<File>, _: usize, _: usize, max_value: usize) -> Vec<Pixel> {
        f.lines()
            .map(|x| x.unwrap())
            .par_bridge()
            .flat_map(|x| -> Vec<Pixel> { LineOfPixel::new(x, max_value).collect() })
            .collect()
    }
}

impl Image {
    //use std::sync::atomic::{AtomicUsize, Ordering};
    //use std::sync::mpsc::Sender;
    //use std::sync::{Arc, mpsc, Mutex};
    //use std::thread;
    //    fn get_par_pixels_vec(mut f: BufReader<File>, width: usize, height: usize, max_value: usize) -> Vec<Pixel> {
    //        let (mut pixels_per_line, mut pixels) = Self::process_first_line(&mut f, width, height, max_value);
    //        let reader_arc = Arc::new(Mutex::new((f, 0_usize)));
    //        let (sender, receiver) = mpsc::channel();
    //        let pixels_per_line = (pixels_per_line + 1) / 3;
    //
    //        for _ in 0..num_cpus::get() {
    //            let reader_arc_clone = reader_arc.clone();
    //            let sender_clone = sender.clone();
    //            thread::spawn(move || {
    //                Self::par_process_line(sender_clone, reader_arc_clone, max_value)
    //            });
    //        }
    //
    //        let mut pixels_counter: usize = 0;
    //
    //        while pixels_counter < width * height {
    //            match receiver.recv() {
    //                Ok((pixel, line_number, relative_index)) => {
    //                    println!("Received : {}, {}, {}", pixel.display(), line_number, relative_index);
    //                },
    //                Err(_) => panic!("Worker threads disconnected before the solution was found!"),
    //            };
    //        }
    //        pixels
    //    }
    //
    //    fn par_process_line(
    //        sender: Sender<(Pixel, usize, usize)>,
    //        reader_arc: Arc<Mutex<(BufReader<File>, usize)>>,
    //        max_value: usize
    //    ) {
    //        let mut buf = String::with_capacity(70);
    //
    //        loop {
    //            let mut mutex_guard = reader_arc.lock().unwrap();
    //            let mut reader = &mut (*mutex_guard).0;
    //            let line_number = (*mutex_guard).1;
    //            if reader.read_line(&mut buf).unwrap() == 0 {
    //                return
    //            }
    //            *mutex_guard = (*reader, line_number + 1);
    //            std::mem::drop(mutex_guard);
    //
    //            let mut relative_index : usize = 0;
    //            for pixel in LineOfPixel::new(buf.clone(), max_value)  {
    //                sender.send((pixel, line_number, relative_index));
    //                relative_index += 1;
    //            }
    //        }
    //    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;
    use test::Bencher;

    const SMALL_FILE: &str = "images/177013_P3.ppm";
    const LARGE_FILE: &str = "images/big_pepe_for_the_queen_P3.ppm";

    #[test]
    fn test_image_ppm_type() {
        let image = Image::new_with_file(Path::new(SMALL_FILE).to_owned());
        assert_eq!(image.ppm_type, "P3")
    }

    #[test]
    fn test_image_height() {
        let image = Image::new_with_file(Path::new(SMALL_FILE).to_owned());
        assert_eq!(image.height, 7)
    }

    #[test]
    fn test_image_width() {
        let image = Image::new_with_file(Path::new(SMALL_FILE).to_owned());
        assert_eq!(image.width, 34)
    }

    #[test]
    fn test_image_max_value() {
        let image = Image::new_with_file(Path::new(SMALL_FILE).to_owned());
        assert_eq!(image.max_value, 255)
    }

    #[bench]
    fn bench_new_with_file_small_file(b: &mut Bencher) {
        b.iter(|| Image::new_with_file(Path::new(SMALL_FILE).to_owned()))
    }

    #[bench]
    fn bench_par_new_with_file_small_file(b: &mut Bencher) {
        b.iter(|| Image::par_new_with_file(Path::new(SMALL_FILE).to_owned()))
    }

    #[bench]
    fn bench_par_new_with_file2_small_file(b: &mut Bencher) {
        b.iter(|| Image::par_new_with_file2(Path::new(SMALL_FILE).to_owned()))
    }

    #[bench]
    fn bench_new_with_file_large_file(b: &mut Bencher) {
        b.iter(|| Image::new_with_file(Path::new(LARGE_FILE).to_owned()))
    }

    #[bench]
    fn bench_par_new_with_file_large_file(b: &mut Bencher) {
        b.iter(|| Image::par_new_with_file(Path::new(LARGE_FILE).to_owned()))
    }

    #[bench]
    fn bench_par_new_with_file2_large_file(b: &mut Bencher) {
        b.iter(|| Image::par_new_with_file2(Path::new(LARGE_FILE).to_owned()))
    }
}
