#[derive(Clone, Copy, Debug)]
pub struct Pixel(u8, u8, u8);

impl Pixel {
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self(red, green, blue)
    }

    #[allow(dead_code)]
    pub fn new_from_slice(v: &[u8]) -> Self {
        Self::new(v[0], v[1], v[2])
    }

    pub fn red(&self) -> u8 {
        self.0
    }

    pub fn green(&self) -> u8 {
        self.1
    }

    pub fn blue(&self) -> u8 {
        self.2
    }

    #[allow(dead_code)]
    pub fn display(&self) -> String {
        format!("Pixel({}, {}, {})", self.0, self.1, self.2)
    }

    pub fn display_raw(&self) -> String {
        format!("{} {} {} ", self.0, self.1, self.2)
    }

    pub fn invert(&mut self) {
        self.0 = !self.0;
        self.1 = !self.1;
        self.2 = !self.2;
    }

    #[allow(dead_code)]
    pub fn to_invert(&self) -> Self {
        Self(!self.0, !self.1, !self.2)
    }

    fn compute_grayscale(&self) -> u8 {
        ((self.0 as u16 + self.1 as u16 + self.2 as u16) / 3) as u8
    }

    pub fn grayscale(&mut self) {
        let mean = self.compute_grayscale();
        self.0 = mean;
        self.1 = mean;
        self.2 = mean;
    }

    #[allow(dead_code)]
    pub fn to_grayscale(&self) -> Self {
        let mean = self.compute_grayscale();
        Self(mean, mean, mean)
    }
}

impl PartialEq for Pixel {
    fn eq(&self, other: &Self) -> bool {
        self.red() == other.red() && self.green() == other.green() && self.blue() == other.blue()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;
    const P: Pixel = Pixel(253, 252, 251);

    #[test]
    fn test_new() {
        assert_eq!(P, Pixel::new(253, 252, 251));
    }

    #[test]
    fn test_new_from_slice() {
        assert_eq!(P, Pixel::new_from_slice(&vec![253, 252, 251]));
    }

    #[test]
    fn test_red() {
        assert_eq!(P.red(), 253);
    }

    #[test]
    fn test_green() {
        assert_eq!(P.green(), 252);
    }

    #[test]
    fn test_blue() {
        assert_eq!(P.blue(), 251);
    }

    #[test]
    fn test_display() {
        assert_eq!(P.display(), "Pixel(253, 252, 251)");
    }

    #[test]
    fn test_display_raw() {
        assert_eq!(P.display_raw(), "253 252 251 ");
    }

    #[test]
    fn test_invert() {
        let mut pp = P;
        let p = pp.invert();
        assert_eq!(p, ());
        assert_eq!(pp, Pixel(2, 3, 4));
    }

    #[test]
    fn test_to_invert() {
        assert_eq!(P.to_invert(), Pixel(2, 3, 4));
    }

    #[test]
    fn test_grayscale() {
        let mut pp = P;
        assert_eq!(pp.grayscale(), ());
        assert_eq!(pp, Pixel(252, 252, 252));
    }

    #[test]
    fn test_to_grayscale() {
        assert_eq!(P.to_grayscale(), Pixel(252, 252, 252));
    }

    #[test]
    fn test_partialeq() {
        assert!(P == Pixel(253, 252, 251));
    }

    #[bench]
    fn bench_new(b: &mut Bencher) {
        let n = test::black_box(253);
        b.iter(|| return Pixel::new(n, 252, 251))
    }

    #[bench]
    fn bench_new_from_slice(b: &mut Bencher) {
        let v = test::black_box(vec![253, 252, 251]);
        b.iter(|| return Pixel::new_from_slice(&v))
    }
}
