use crate::pixel::Pixel;
use std::collections::VecDeque;

pub struct LineOfPixel {
    iter_line: VecDeque<u8>,
}

impl LineOfPixel {
    pub fn new(line: String, max_value: usize) -> Self {
        Self {
            iter_line: line
                .split_whitespace()
                .map(|x| Self::string_to_normalised_value(x, max_value))
                .collect(),
        }
    }

    /// Normalise value  of pixel to 255
    pub fn normalise_value(value: usize, max_value: usize) -> u8 {
        (value as f64 / max_value as f64 * 255.0) as u8
    }

    fn string_to_normalised_value(s: &str, max_value: usize) -> u8 {
        Self::normalise_value(s.trim().parse::<usize>().unwrap(), max_value)
    }
}


/// Return a Pixel in Iterator
impl Iterator for LineOfPixel {
    type Item = Pixel;

    fn next(&mut self) -> Option<Pixel> {
        match self.iter_line.pop_front() {
            Some(subpixel) => Some(Pixel::new(
                subpixel,
                self.iter_line.pop_front().unwrap(),
                self.iter_line.pop_front().unwrap(),
            )),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn test_new() {
        let lop: Vec<Pixel> = LineOfPixel::new("253 252 251".to_string(), 255).collect();
        assert_eq!(
            lop,
            vec![Pixel::new(253, 252, 251)]
        );
    }

    #[test]
    fn test_normalise() {
        let n_value = LineOfPixel::normalise_value(500, 1000);
        assert_eq!(n_value, 127);
    }

    #[test]
    fn test_string_normalise() {
        let n_value = LineOfPixel::string_to_normalised_value("500", 1000);
        assert_eq!(n_value, 127);
    }

    #[test]
    fn test_next_iter() {
        let mut pixels: Pixel = Pixel::new(0, 0, 0);

        for pixel in LineOfPixel::new("993 989 985".to_string(), 1000) {
            pixels = pixel;
        }
        assert_eq!(pixels, Pixel::new(253,252,251));
    }

    #[bench]
    fn bench_new(b: &mut Bencher) {
        b.iter(|| LineOfPixel::new("253 252 251".to_string(), 255))
    }

    #[bench]
    fn bench_normalise(b: &mut Bencher) {
        b.iter(|| LineOfPixel::normalise_value(500, 1000))
    }

    #[bench]
    fn bench_string_normalise(b: &mut Bencher) {
        b.iter(|| LineOfPixel::string_to_normalised_value("500", 1000))
    }
}

