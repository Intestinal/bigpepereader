extern crate cc;

fn main() {
    println!("cargo:rerun-if-changed=src/ppma_io/ppma_io.c");
    cc::Build::new()
        .file("src/ppma_io/ppma_io.c")
        .include("ppma_io")
        .compile("my_ppma_io");
    // The lib will be called "my_ppma_io" so you can use it like this :
    //   #[link(name = "my_ppma_io")]
}
