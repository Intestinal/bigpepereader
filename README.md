# [BigPepeReader](https://gitlab.com/Intestinal/bigpepereader)
Project members :
- XU Yannick
- SCANU Valentin
- HOLLANDER Stéphane

## Build
```
cargo build
```
You must use Rust nightly.

## Usages
```
./bigpepereader <image-path> <output-path> --function <function>
```
For more information :
```
./bigpepereader --help  
```

## Install library as Python package
```
python setup.py intall
```

## Test
```
cargo test --package bigpepereader --bin bigpepereader
```

## Benchmark
```
cargo bench --package bigpepereader --bin bigpepereader
```